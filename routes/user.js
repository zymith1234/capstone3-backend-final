const User = require("../models/User")
const UserController = require("../controllers/user")
const express = require("express")
const router = express.Router()
const auth = require("../auth")

//get details of user
router.get('/details', auth.verify, (req,res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.get({userId: user.id}).then(user => res.send(user))
})

//get the all records/transactions of the user
router.get('/details/records', auth.verify, (req,res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.getRecords({userId: user.id}).then(user => res.send(user))
})

//get the all categories of the user
router.get('/details/categories', auth.verify, (req,res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.getCategories({userId: user.id}).then(user => res.send(user))
})

//register the guest manually
router.post("/", (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//check if email exist for manual login users
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))	
})

//login
router.post('/login', (req,res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//login or register using google login
router.post('/verify-google-id-token', async(req,res)=>{
	res.send(await UserController.verifyGoogleTokenId(req.body))
})

//add a new category
router.post('/new-category', auth.verify, (req,res)=>{
	let userData = auth.decode(req.headers.authorization) 
	let params = {
		userId: userData.id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType
	}
	UserController.newCategory(params).then(resultFromNewCateg => res.send(resultFromNewCateg))
})

//add a new record transaction
router.post('/new-record', auth.verify, (req,res)=>{
	let userData = auth.decode(req.headers.authorization)
	let params = {
		userId: userData.id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType,
		description: req.body.description,
		amount: req.body.amount
	}
	UserController.newRecord(params).then(resultFromNewRec => res.send(resultFromNewRec))
})

//update the info of the user
router.put('/', auth.verify , (req,res) => {
	UserController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})



module.exports = router