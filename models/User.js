const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	loginType: {
		type: String,
		required: [true, 'Login Type is required']
	},
	categories: [{
		categoryName: {
			type: String,
			required: [true, "Category Name is required."]
		},
		categoryType: {
			type: String,
			required: [true, "Type is required."]
		}
	}],
	records: [{
		categoryName: {
			type: String,
			required: [true, "Category Name is required."]
		},
		categoryType: {
			type: String,
			required: [true, "Type is required."]
		},
		inputOn: {
			type: Date, 
			default: new Date() 
		},
		amount: {
			type: Number,
			required: [true, "Amount is required."]
		},
		description: {
			type: String,
			required: [true, "Description is required."]
		}	
	}]
})

module.exports = mongoose.model('user', userSchema)