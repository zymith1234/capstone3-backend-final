const express = require('express');
const app = express(); 
const mongoose = require(`mongoose`) 
const cors = require(`cors`) 

app.use(express.json())
app.use(express.urlencoded({ extended:true })) 
app.use(cors())

mongoose.connection.once(`open`, () => console.log(`Now connected to MongoDB Atlas.`))
mongoose.connect(`mongodb+srv://zymith1234:lotusclan1234@cluster0.zg4o7se.mongodb.net/budget-tracking-capstone3?retryWrites=true&w=majority&appName=Cluster0`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

const userRoutes = require(`./routes/user`)
app.use(`/api/users`, userRoutes) 

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
}) 

