const User = require("../models/User")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const {OAuth2Client} = require('google-auth-library')
const clientId = '38077020784-v9t8p0lrj6qllvfadttb69qu9sic2ag4.apps.googleusercontent.com'
const { google } = require("googleapis")
const OAuth2 = google.auth.OAuth2
const nodemailer = require("nodemailer")
require('dotenv').config()

//check if the person who register manually has the same email on other users
module.exports.emailExists = (parameterFromRoute) => {
	return User.find({ email: parameterFromRoute.email }).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
}

//register manually
module.exports.register = async (params) => {
	const salt = await bcrypt.genSalt(10)
	const hashedPassword = await bcrypt.hash(params.password, salt)
	console.log('params.password --', params.password);
	console.log('hashedPassword', hashedPassword);
	let newUser = await new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		loginType: "Not Google",
		password: hashedPassword
	})
	console.log('newUser --', newUser)
	return await newUser.save().then((user, err)=>{
		return (err) ? false : true
	})
}


//login manually
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if (resultFromFindOne == null){
			return false
		}
		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
		if(isPasswordMatched){
			return {accessToken: auth.createAccessToken(resultFromFindOne)}
		} else {
			return false
		}
	})
}

//get the details of the user
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}

//update the info details of user
module.exports.update = (params) => {
	let updateDetails = {
		firstName: params.firstName,
		lastName: params.lastName
	}
	return User.findByIdAndUpdate(params.id, updateDetails).then((user,err) => {
		return (err) ? false : true
	})
}

//verify the google login token 
module.exports.verifyGoogleTokenId = async (body) => {
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: body.tokenId, audience: clientId})
	if (data.payload.email_verified === true) {
		const user = await User.findOne({email: data.payload.email}).exec()
		if(user !== null){
			console.log('A user with the same email has been registered.')
			if(user.loginType === "google"){
				return {accessToken: auth.createAccessToken(user)}
			} else {
				return {error: 'login-type-error'}
			}
		} else {
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				password: null,
				loginType: "google"
			})
			return newUser.save().then((user, err)=>{
				return {accessToken: auth.createAccessToken(user)}
			})
		}
	} else {
		return {error: "google-auth-error"}
	}
}

//add a new category
module.exports.newCategory = (params) => {
	return User.findById(params.userId).then(resultFromFindByIdUser => {
		resultFromFindByIdUser.categories.push({
			categoryName: params.categoryName,
			categoryType: params.categoryType
		})
		return resultFromFindByIdUser.save().then((result,err)=>{
			return (err) ? false : true
		})
	})
}

//add a new record
module.exports.newRecord = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.records.push({
			categoryName: params.categoryName,
			categoryType: params.categoryType,
			amount: params.amount,
			description: params.description
			
		})
		return resultFromFindById.save().then((result,err)=>{
			return (err) ? false : true
		})
	})
}

//get all the user's categories
module.exports.getCategories = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		return resultFromFindById.categories
	})
}

//get all the user's records
module.exports.getRecords = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		return resultFromFindById.records
	})
}

